package chupin.projet.projet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import chupin.projet.projet.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}